package com.example.tablayout

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.example.tablayout.Global.userDetails
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        val viewPager = findViewById<ViewPager>(R.id.viewPager_account)
        val tabLayout = findViewById<TabLayout>(R.id.tabLayout_account)

        viewPager.adapter = TabsAdapter(supportFragmentManager)

        tabLayout.setupWithViewPager(viewPager)

        val button=findViewById<FloatingActionButton>(R.id.fab)
        button.setOnClickListener {
            val intent= Intent(this,MainActivity2::class.java)
            startActivity(intent)

        }


    }
    override fun  onCreateOptionsMenu(menu:Menu?): Boolean {
            menuInflater.inflate(R.menu.menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.home -> {
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.setting -> {
                Toast.makeText(this, "Setting", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.group ->{
                Toast.makeText(this, "New Group", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.payment ->{
                Toast.makeText(this, "Payment", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.device ->{
                Toast.makeText(this, "Linked device", Toast.LENGTH_SHORT).show()
                true
            }
            else ->  super.onOptionsItemSelected(item)


        }
    }

}
