package com.example.tablayout

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Email
import android.widget.ArrayAdapter
import android.widget.ListView

class custom_Item : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_item)




        val userName=intent.extras?.get("Name")
        val userEmail=intent.extras?.get("Email")


        val userArrayAdapter=
            ArrayAdapter(this, com.bumptech.glide.R.layout.support_simple_spinner_dropdown_item,
               Global.userDetails
            )
        val userListview=findViewById<ListView>(R.id.list)
        userListview.adapter=userArrayAdapter


        Global.userDetails.add(CustomListModel(userName.toString()+userEmail.toString()))


    }
}




