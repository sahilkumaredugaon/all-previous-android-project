package com.example.tablayout

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        val name = findViewById<EditText>(R.id.name)
        val email = findViewById<EditText>(R.id.email)
        val button = findViewById<Button>(R.id.button)


        button.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("Name", name.text.toString())
            intent.putExtra("Email", email.text.toString())
            startActivity(intent)

        }

    }}