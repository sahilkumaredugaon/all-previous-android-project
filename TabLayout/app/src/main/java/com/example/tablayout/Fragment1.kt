package com.example.tablayout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView

class Fragment1 : Fragment() {
    val customDataList = datalist.names
    lateinit var cusListView:ListView
    lateinit var customListAdapter: CustomAdaptor
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rowView =inflater.inflate(R.layout.fragment_1, container, false)
        cusListView  = rowView.findViewById<ListView>(R.id.cusList)
        customListAdapter = activity?.let { CustomAdaptor(Global.userDetails, it) }!!
        cusListView.adapter = customListAdapter

        return  rowView
    }

    override fun onResume() {
        super.onResume()
        customListAdapter.notifyDataSetChanged()
    }
}

//class Fragment1 : Fragment() {
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        // Inflate the layout for this fragment
//        val rootView= inflater.inflate(R.layout.fragment_1, container, false)
//
//
//
//
//        val cusListView = rowView.findViewById<ListView>(R.id.cusList)
////        cusListView.adapter = activity?.let { CustomAdaptor(customDataList, it) }
//
//        return  rootView
//    }
//
//}