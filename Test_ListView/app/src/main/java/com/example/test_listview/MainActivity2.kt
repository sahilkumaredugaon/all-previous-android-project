package com.example.test_listview

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import com.example.test_listview.Global.UserName

class MainActivity2 : AppCompatActivity() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main2)

            val textName = intent.extras?.get("Name")
            val textEmail = intent.extras?.get("Email")
            val radioButton = intent.extras?.get("radioButton")
            val spinner = intent.extras?.get("Spinner")

            val listAdapterView = ArrayAdapter(this,
                com.google.android.material.R.layout.select_dialog_item_material,
                UserName
            )
            val userList = findViewById<ListView>(R.id.UserList)
            userList.adapter = listAdapterView

            UserName.add("Name:-" + textName.toString() + "\n" + "Email:-" + textEmail.toString() + "\n" + "Gender:-" + radioButton.toString() + "\n" + "State:-" + spinner.toString())


        userList.setOnItemClickListener { adapterView, view, i, l ->
            val builder = android.app.AlertDialog.Builder(this)
            builder.setTitle("Delete")
            builder.setMessage("Are You Sure Delete it?")
            builder.setIcon(R.drawable.ic_baseline_delete_forever_24)
            builder.setPositiveButton("Yes") { _, _ ->
                val itemValue = userList.getItemAtPosition(i)
                UserName.remove(itemValue)
                listAdapterView.notifyDataSetChanged()

            }
            builder.setNegativeButton("No") { dialoginterface, which ->
            }
            builder.setNeutralButton("Ok") { dialoginterface, which ->

            }
            val alertDialog = builder.create()
            alertDialog.setCancelable(true)
            alertDialog.show()


        }

        }
}




