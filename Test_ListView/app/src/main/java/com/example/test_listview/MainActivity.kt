package com.example.test_listview

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Spinner
import android.widget.Toast
import androidx.core.view.isNotEmpty
import java.util.jar.Attributes.Name

class MainActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val spinnerArray= arrayListOf("Select State","Bihar","Jharkhand","Goa",
            "Lucknow","Punjab","Andhra Pradesh","Arunachal Pradesh","Chhattisgarh","Gujarat")
        val spinnerAdapter=ArrayAdapter(this, androidx.constraintlayout.widget.R.layout.support_simple_spinner_dropdown_item,spinnerArray)
        val spinner=findViewById<Spinner>(R.id.spinner1)
        spinner.adapter=spinnerAdapter

        spinner.onItemSelectedListener= object:AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                Toast.makeText(this@MainActivity, spinnerArray[p2], Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                
            }

        }


        val textName=findViewById<EditText>(R.id.textName)
        val textEmail=findViewById<EditText>(R.id.textEmail)
        val radioGroup=findViewById<RadioGroup>(R.id.radioGroup)
        val button=findViewById<Button>(R.id.button)


        button.setOnClickListener {
            if(textName.text.isNotEmpty()&& textName.text.isNotEmpty()&& radioGroup.isNotEmpty()&& spinner.isNotEmpty()) {
                val selectOption: Int = radioGroup!!.checkedRadioButtonId
                val radioButton = findViewById<Button>(selectOption)

                val intent=Intent(this,MainActivity2::class.java)
                intent.putExtra("Name",textName.text.toString())
                intent.putExtra("Email",textEmail.text.toString())
                intent.putExtra("radioButton",radioButton.text.toString())
                intent.putExtra("Spinner",spinner.selectedItem.toString())

                Toast.makeText(this, textName.text.toString()+textEmail.text.toString()
                        +radioButton.text.toString()+spinner.selectedItem.toString(), Toast.LENGTH_SHORT).show()
                startActivity(intent)
            }

        }

    }
}