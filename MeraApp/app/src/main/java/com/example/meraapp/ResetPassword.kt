package com.example.meraapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class ResetPassword : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        val resetPassword=findViewById<EditText>(R.id.reset)
        val confirmPassword=findViewById<EditText>(R.id.confirm)
        val changePassword=findViewById<Button>(R.id.passwordButton)



        changePassword.setOnClickListener {
            if (resetPassword.text.isNotEmpty() && confirmPassword.text.isNotEmpty()) {
                if (resetPassword.text.toString() == confirmPassword.text.toString()) {
                    val regSharePreferences = getSharedPreferences("register", Context.MODE_PRIVATE)
                    val  editPreferences = regSharePreferences.edit()

                    editPreferences.putString("password", resetPassword.text.toString()).commit()

                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                        finish()
                    Toast.makeText(this, "Reset Password Successful", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Please Correct Same Password", Toast.LENGTH_SHORT).show()
                }

            }
            else{
                Toast.makeText(this, "Please Fill Up All Data", Toast.LENGTH_SHORT).show()
            }
        }
    }
}