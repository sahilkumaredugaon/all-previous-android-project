package com.example.meraapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.meraapp.databinding.ActivityMainBinding
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {

//    var db= FirebaseFirestore.getInstance()

    private  lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.regButton.setOnClickListener {
            startActivity(Intent(this,RegisterActivity::class.java) )
            finish()
        }
        binding.logButton.setOnClickListener {
            startActivity(Intent(this,LoginActivity::class.java) )
            finish()
        }



//        db.collection("users").get().addOnSuccessListener {
//            val document= it.documents[0]
//            var id=document.id
//            var name=document.data?.get("name")
//            var email=document.data?.get("email")
//            var password=document.data?.get("password")
//        }
    }
}