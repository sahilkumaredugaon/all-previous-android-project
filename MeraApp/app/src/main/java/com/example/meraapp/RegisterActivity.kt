package com.example.meraapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.google.firebase.firestore.FirebaseFirestore

class RegisterActivity : AppCompatActivity() {

    var firebasefirestore= FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)



        val clickText=findViewById<TextView>(R.id.textLogin)
        clickText.setOnClickListener {
            val intent=Intent(this,LoginActivity::class.java)
            startActivity(intent)
        }


        val regSharePreferences = getSharedPreferences("register", Context.MODE_PRIVATE)
        val logSharePreferences = getSharedPreferences("login", Context.MODE_PRIVATE)
        // make editable share preference
        val  editPreferences = regSharePreferences.edit()

        val userName = findViewById<EditText>(R.id.userName)
        val textEmail = findViewById<EditText>(R.id.email)
//        val showDetailsTextView = findViewById<TextView>(R.id.showDetails_TextView)
        val password=findViewById<EditText>(R.id.password)

        val submitButton = findViewById<Button>(R.id.button_submit)



        // set shaved text in text view
//        showDetailsTextView.text = "$savedName, $savedEmail"
        submitButton.setOnClickListener {


            if (userName.text.isNotEmpty()&& textEmail.text.isNotEmpty() && password.text.isNotEmpty()) {

                val name=userName.text.toString().trim()
                val email=textEmail.text.toString().trim()
                val password= password.text.toString().trim()

                val userMap= hashMapOf(
                    "user_name" to name,
                     "email"  to email,
                    "password" to password
                )

            firebasefirestore.collection("users").document().set(userMap)
                .addOnSuccessListener {
                    Toast.makeText(this, "Data Inset Successful", Toast.LENGTH_SHORT).show()



                    logSharePreferences.edit().putBoolean("LoginStatusKey", true).commit()
                    val intent = Intent(this, LogOutActivity::class.java)

                    // add name and email into sharePref
                    editPreferences.putString("name", userName.text.toString())
                    editPreferences.putString("email", textEmail.text.toString())
                    editPreferences.putString("password", password)

                    // save all added data in share pref
                    editPreferences.commit()
                    startActivity(intent)
                    finish()


                }
                .addOnFailureListener {
                    Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                }
            }

            else{
                Toast.makeText(this, "Please Fill Up All Data", Toast.LENGTH_SHORT).show()
            }
        }

    }
}