package com.example.meraapp

import android.content.ClipData.Item
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView

class PraticeNavigationButton : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pratice_navigation_button)

        val fragment1=Fragment1()
        val fragment2=Fragment2()
        val fragment3=Fragment3()

        val fragmentManager=supportFragmentManager.beginTransaction()
        fragmentManager.replace(R.id.frame_Layout,fragment1).commit()

        val button=findViewById<BottomNavigationView>(R.id.BottomNavigationView)
        button.setOnItemSelectedListener {
            when(it.itemId){
                R.id.home->{
                    supportFragmentManager.beginTransaction().replace(R.id.frame_Layout,fragment1).commit()
                }
                R.id.setting->{
                    supportFragmentManager.beginTransaction().replace(R.id.frame_Layout,fragment2).commit()
                }
                R.id.profile->{
                    supportFragmentManager.beginTransaction().replace(R.id.frame_Layout,fragment3).commit()
                }

            }
            true
        }



    }
}


