package com.example.meraapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity

class SplaceMainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_space_main2)

        val logSharePreferences = getSharedPreferences("login", Context.MODE_PRIVATE)
          val loginStatus=logSharePreferences.getBoolean("LoginStatusKey",false)

        // make editable share preference

        Handler().postDelayed({

            if (loginStatus==true){
                val intent= Intent(this,LogOutActivity::class.java)
                startActivity(intent)
                finish()
                Toast.makeText(this, "WelCome To HomePage", Toast.LENGTH_SHORT).show()

            }
            else{
                val intent= Intent(this,MainActivity::class.java)
                startActivity(intent)
                finish()
                Toast.makeText(this, "WelCome To App", Toast.LENGTH_SHORT).show()

            }
        },3000)






    }


}