package com.example.meraapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

class LogOutActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_out)


        val home=Fragment1()
        val profile=Fragment2()
        val setting=Fragment3()

        setCurrentFragment(home)

        val navButton=findViewById<BottomNavigationView>(R.id.BottomNavigationView)

        navButton.setOnItemSelectedListener{
            when(it.itemId){
                R.id.home->setCurrentFragment(home)
                R.id.profile->setCurrentFragment(profile)
                R.id.setting->setCurrentFragment(setting)
            }
            true
        }
    }

    private fun setCurrentFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_Layout,fragment)
            commit()
        }





    }

}