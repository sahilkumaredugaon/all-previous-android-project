package com.example.meraapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        val forgetPassword=findViewById<TextView>(R.id.forgetPassword)
        forgetPassword.setOnClickListener {
            val intent=Intent(this,ResetPassword::class.java)
            startActivity(intent)
        }

        val clickText=findViewById<TextView>(R.id.registerText)
        clickText.setOnClickListener {
            val intent=Intent(this,RegisterActivity::class.java)
            startActivity(intent)
            finish()
        }

        val sharePreferences = getSharedPreferences("register", Context.MODE_PRIVATE)

        // make editable share preference
        val  editPreferences = sharePreferences.edit()
        // get saved data from share pref
        val  savedName = sharePreferences.getString("name", "")
        val  savedPassword = sharePreferences.getString("password", "")


        val loginUserName=findViewById<EditText>(R.id.userNameLogin)
        val  loginPassword=findViewById<EditText>(R.id.LoginPassword)
        val button=findViewById<Button>(R.id.loginButton)

        button.setOnClickListener {

            if(loginUserName.text.isNotEmpty() && loginPassword.text.isNotEmpty()){


            if (loginUserName.text.toString() == savedName.toString() && loginPassword.text.toString() == savedPassword.toString()) {

                val logSharePreferences = getSharedPreferences("login", Context.MODE_PRIVATE)
                logSharePreferences.edit().putBoolean("LoginStatusKey",true).commit()

                val intent = Intent(this, LogOutActivity::class.java)
                startActivity(intent)
                finish()

                Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show()

            }
            else{
                Toast.makeText(this, "Please Correct Username And Password", Toast.LENGTH_SHORT).show()
            }
            }
            else{
                Toast.makeText(this, "Please Fill Up All Data", Toast.LENGTH_SHORT).show()
            }
        }
    }
}