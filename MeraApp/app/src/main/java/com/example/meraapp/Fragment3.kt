package com.example.meraapp

import android.content.Context
import android.content.Intent
import android.content.Intent.EXTRA_INTENT
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore

class Fragment3 : Fragment() {

    var db= FirebaseFirestore.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView= inflater.inflate(R.layout.fragment_3, container, false)

        val showData = rootView.findViewById<TextView>(R.id.showDetails_TextView)

        val regSharePreferences = activity?.getSharedPreferences("register", Context.MODE_PRIVATE)
        val editPreferences = regSharePreferences?.edit()
        val savedName = regSharePreferences?.getString("name", "")
        val savedEmail = regSharePreferences?.getString("email", "")
        val savedPassword = regSharePreferences?.getString("password", "")

        showData.text = savedName.toString() + "\n" + savedEmail.toString()


        val button = rootView.findViewById<Button>(R.id.logout)

        button.setOnClickListener {
            activity.let {
                val intent = Intent(it, MainActivity::class.java)
                startActivity(intent)
                activity?.finish()
                Toast.makeText(activity, "LogOut Successful!", Toast.LENGTH_SHORT).show();
                val logSharePreferences = activity?.getSharedPreferences("login", Context.MODE_PRIVATE)
                logSharePreferences?.edit()?.putBoolean("LoginStatusKey", false)?.commit()
            }

        }



        val textView=rootView.findViewById<TextView>(R.id.usersName)

        db.collection("users").get().addOnSuccessListener {
            val document= it.documents[0]
            val id=document.id
            val name=document.data?.get("user_name")
            val email=document.data?.get("email")
            val password=document.data?.get("password")

            textView.text=id.toString()+"\n"+name.toString()+"\n" +email.toString()+"\n"+password.toString()


        }

        return rootView

    }

}