package com.example.authentication

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Email
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {

  lateinit var auth:FirebaseAuth

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


       auth=FirebaseAuth.getInstance()

        val emailEditText=findViewById<EditText>(R.id.user_Email)
        val passwordEditText=findViewById<EditText>(R.id.user_Password)
        val button=findViewById<Button>(R.id.Button)

        button.setOnClickListener {
        auth.createUserWithEmailAndPassword(emailEditText.text.toString(),passwordEditText.text.toString())
            .addOnSuccessListener {

                val intent=Intent(this,HomeActivity::class.java)
                startActivity(intent)
                Toast.makeText(this, "Registration Successful", Toast.LENGTH_SHORT).show()

                finish()
            }
            .addOnFailureListener{
                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
            }


        }

        val loginText=findViewById<TextView>(R.id.loginText)

        loginText.setOnClickListener {
            auth.signInWithEmailAndPassword(emailEditText.text.toString(),passwordEditText.text.toString())
                .addOnSuccessListener {

                    val intent=Intent(this,HomeActivity::class.java)
                    startActivity(intent)
                    Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show()
                    finish()
                }
                .addOnFailureListener{
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }

        }

    }
}