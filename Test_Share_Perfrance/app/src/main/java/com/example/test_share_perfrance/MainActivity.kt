package com.example.test_share_perfrance

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton

class MainActivity : AppCompatActivity() {
    @SuppressLint("CutPasteId", "WrongViewCast", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val sharePreference=getSharedPreferences("user", Context.MODE_PRIVATE)
//        val editPreferences=sharePreference.edit()
//
//        val saveName=sharePreference.getString("name","")
//        val saveEmail=sharePreference.getString("number","")
//
//        val editName=findViewById<EditText>(R.id.edit_Name)
//        val editNumber=findViewById<EditText>(R.id.edit_Number)
//        val button=findViewById<AppCompatButton>(R.id.button)
//        val showData=findViewById<TextView>(R.id.showDetails_TextView)
//
//        showData.text="$editName,$editNumber"
//
//        button.setOnClickListener {
//            val details=editName.text.toString()+"\n"+editNumber.text.toString()
//            showData.text=details
//
//            editPreferences.putString("name",editName.text.toString())
//            editPreferences.putString("number",editNumber.text.toString())
//
//            editPreferences.apply()
//
//        }

        val sharePreferences = getSharedPreferences("user", Context.MODE_PRIVATE)
        // make editable share preference
        val  editPreferences = sharePreferences.edit()
        // get saved data from share pref
        val  savedName = sharePreferences.getString("name", "")
        val  savedEmail = sharePreferences.getString("number", "")

        // get location of view ( TextView, EditText and Button ) from xml
        val nameEditText = findViewById<EditText>(R.id.edit_Name)
        val emailEditText = findViewById<EditText>(R.id.edit_Number)
        val showDetailsTextView = findViewById<TextView>(R.id.showDetails_TextView)
        val submitButton = findViewById<AppCompatButton>(R.id.button)

        // set shaved text in text view
        showDetailsTextView.text = "$savedName \n $savedEmail"

        submitButton.setOnClickListener {

            val details = nameEditText.text.toString() + "\n" + emailEditText.text.toString()
            // set text from editText in text showDetailsTextView
            showDetailsTextView.text = details

            // add name and email into sharePref
            editPreferences.putString("name", nameEditText.text.toString())
            editPreferences.putString("email", emailEditText.text.toString())

            // save all added data in share pref
            editPreferences.commit()
        }



    }
}