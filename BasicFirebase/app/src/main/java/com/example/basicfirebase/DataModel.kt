package com.example.basicfirebase

import android.icu.text.CaseMap.Title

data class DataModel(
    val title: String?=null,
    val price: Int?=null,
    val description: String?=null,
    val image:String?=null

)
