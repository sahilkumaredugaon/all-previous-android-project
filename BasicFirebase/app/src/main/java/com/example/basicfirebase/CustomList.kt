package com.example.basicfirebase

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ListAdapter
import android.widget.TextView
import com.bumptech.glide.Glide

class CustomList(val arrayList: ArrayList<DataModel>,val context: Context) :BaseAdapter() {
    override fun getCount(): Int {
      return  arrayList.size
    }

    override fun getItem(position: Int): Any {
      return arrayList[position]
    }

    override fun getItemId(position: Int): Long {
      return  arrayList.size.toLong()
    }

    @SuppressLint("ViewHolder", "MissingInflatedId")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val root = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false)

        val title = root.findViewById<TextView>(R.id.title)
        val price = root.findViewById<TextView>(R.id.price)
        val description = root.findViewById<TextView>(R.id.description)
        val image = root.findViewById<ImageView>(R.id.image)

        title.text = arrayList[position].title
        price.text = arrayList[position].price.toString()
        description.text = arrayList[position].description
        val url = arrayList[position].image

        Glide.with(context)
            .load(url).error(R.drawable.ic_launcher_background).into(image)

        return root


    }
}


