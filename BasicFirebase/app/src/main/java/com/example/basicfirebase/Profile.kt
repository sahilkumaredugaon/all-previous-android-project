package com.example.basicfirebase

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.firestore.FirebaseFirestore

class Profile : AppCompatActivity() {


    @SuppressLint("SetTextI18n", "CommitPrefEdits", "MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile)


        val fragment1=HomeFragment()
        val fragment2=MessageFragment()
        val fragment3=ProfileFragment()

        val fragmentManager=supportFragmentManager.beginTransaction()
        fragmentManager.replace(R.id.frame_Layout,fragment1).commit()

        val button=findViewById<BottomNavigationView>(R.id.BottomNavigationView)
        button.setOnItemSelectedListener {
            when(it.itemId){
                R.id.home->{
                    supportFragmentManager.beginTransaction().replace(R.id.frame_Layout,fragment1).commit()
                }
                R.id.message->{
                    supportFragmentManager.beginTransaction().replace(R.id.frame_Layout,fragment2).commit()
                }
                R.id.profile->{
                    supportFragmentManager.beginTransaction().replace(R.id.frame_Layout,fragment3).commit()
                }

            }
            true
        }




//        val showName=findViewById<TextView>(R.id.saveName)
//        val showEmail=findViewById<TextView>(R.id.showEmail)
//        val showPassword=findViewById<TextView>(R.id.showPassword)
//        val showNumber=findViewById<TextView>(R.id.showNumber)
//
//        val regSharePreferences = getSharedPreferences("register", Context.MODE_PRIVATE)
//        val docid= regSharePreferences.getString("userId", "")
////        val logSharePreferences = getSharedPreferences("login", Context.MODE_PRIVATE)
//
//        val  editPreferences = regSharePreferences.edit()
//
//        firebaseFirestone.collection("sahil").document(docid.toString()).get()
//            .addOnSuccessListener {
//                val name = it.data?.get("name")
//                val email = it.data?.get("email")
//                val number = it.data?.get("number")
//                val password = it.data?.get("password")
//
//
//                showName.text=name.toString()
//                showEmail.text=email.toString()
//                showNumber.text=number.toString()
//                showPassword.text=password.toString()
//
//
//
//
//
//        val button=findViewById<Button>(R.id.Edit)
//        button.setOnClickListener {
//            val intent = Intent(this, EditProfile::class.java)
//
//
//            editPreferences.putString("id",docid.toString())
//            editPreferences.putString("number",number.toString())
//            editPreferences.putString("password",password.toString())
//            editPreferences.putString("name",name.toString())
//            editPreferences.putString("email",email.toString())
//
//            Toast.makeText(this, " Welcome Edit Profile", Toast.LENGTH_SHORT).show()
//            // save all added data in share pref
//            editPreferences.apply()
//            startActivity(intent)
//            finish()
//        }
//
//        }







        }
}