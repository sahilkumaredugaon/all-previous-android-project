package com.example.basicfirebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.google.firebase.auth.FirebaseAuth

class SplaceScreen : AppCompatActivity() {

    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splace_screen)

        auth=FirebaseAuth.getInstance()


        Handler().postDelayed({

            if(auth.currentUser!=null) {

                val intent=Intent(this,Profile::class.java)
                startActivity(intent)
                finish()

            }
            else{
                val intent = Intent(this, Login::class.java)
                startActivity(intent)
                finish()
            }
        },2000)
    }
}