package com.example.basicfirebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import org.checkerframework.common.returnsreceiver.qual.This
import java.util.concurrent.TimeUnit

class PhoneActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private  lateinit var phoneEditText: TextInputEditText
    private  lateinit var  otpEditText: TextInputEditText


    private lateinit var getOptBtn:Button
    private  lateinit var verifyBtn: AppCompatButton


    private  var storeVerificationId=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone)

        auth=FirebaseAuth.getInstance()

        phoneEditText=findViewById(R.id.inputNumber)
        otpEditText=findViewById(R.id.inputOtp)

        getOptBtn=findViewById(R.id.getOptBtn)
        verifyBtn=findViewById(R.id.verifyBtn)


        getOptBtn.setOnClickListener {
            val phoneNumber="+91"+ phoneEditText.text.toString()
            val phoneAuth = PhoneAuthOptions.newBuilder(auth)
                .setPhoneNumber(phoneNumber)
                .setTimeout(60L,TimeUnit.SECONDS)
                .setActivity(this)
                .setCallbacks(object :PhoneAuthProvider.OnVerificationStateChangedCallbacks(){

                    override fun onCodeSent(verificationId: String, p1: PhoneAuthProvider.ForceResendingToken) {
                        Toast.makeText(this@PhoneActivity, "OTP is  sent", Toast.LENGTH_SHORT).show()
                        storeVerificationId=verificationId
                    }

                    override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                        Toast.makeText(this@PhoneActivity, "OTP is  sent", Toast.LENGTH_SHORT).show()

                    }


                    override fun onVerificationFailed(p0: FirebaseException) {
                     Toast.makeText(this@PhoneActivity, "OTP is  sent failed", Toast.LENGTH_SHORT).show()

}


                })
                .build()
            PhoneAuthProvider.verifyPhoneNumber(phoneAuth)
        }


     verifyBtn.setOnClickListener {
         val getOtpEditText=otpEditText.text.toString()
         val credential=PhoneAuthProvider.getCredential(storeVerificationId, getOtpEditText)

         auth.signInWithCredential(credential)
             .addOnSuccessListener {
                 Toast.makeText(this, "Login  Or Verify Successful", Toast.LENGTH_SHORT).show()
                 val intent=Intent(this,Profile::class.java)
                 startActivity(intent)
             }
             .addOnFailureListener{
                 Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show()
             }
     }





    }
}