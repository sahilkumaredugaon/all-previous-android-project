package com.example.basicfirebase

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {

    val firebaseFirestone = FirebaseFirestore.getInstance()

    lateinit var auth:FirebaseAuth

    @SuppressLint("MissingInflatedId", "CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


//        val regSharePreferences = getSharedPreferences("register", Context.MODE_PRIVATE)
//        val logSharePreferences = getSharedPreferences("login", Context.MODE_PRIVATE)
        // make editable share preference
//        val  editPreferences = regSharePreferences.edit()

        auth=FirebaseAuth.getInstance()

        val name = findViewById<EditText>(R.id.userName)
        val email = findViewById<EditText>(R.id.email)
        val password = findViewById<EditText>(R.id.password)
        val number = findViewById<EditText>(R.id.number)

        val button = findViewById<Button>(R.id.button_submit)


        button.setOnClickListener {
//            if (name.text.isNotEmpty() && email.text.isNotEmpty() && password.text.isNotEmpty() && number.text.isNotEmpty())

            auth.createUserWithEmailAndPassword(email.text.toString(),password.text.toString())
                .addOnSuccessListener {


                    val userName = name.text.toString().trim()
                    val userEmail = email.text.toString().trim()
                    val userPassword = password.text.toString().trim()
                    val userNumber = number.text.toString().trim()

                    val allDataDetails = hashMapOf(
                        "name" to userName,
                        "email" to userEmail,
                        "number" to userNumber,
                        "password" to userPassword
                    )


                    firebaseFirestone.collection("sahil").add(allDataDetails)
                        .addOnSuccessListener {
                            val sharedPreferences =
                                getSharedPreferences("register", Context.MODE_PRIVATE).edit()
                            var docid = it.id
                            sharedPreferences.putString("userId", docid).apply()

                            Toast.makeText(this, "Registration Successful", Toast.LENGTH_SHORT)
                                .show()

                            val intent = Intent(this, Profile::class.java)
                            startActivity(intent)
                            finish()
                        }
                }
                        .addOnFailureListener {
                            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                        }




        }

        val loginText=findViewById<TextView>(R.id.loginText)
        loginText.setOnClickListener {
            val intent=Intent(this,Login::class.java)
            startActivity(intent)
        }

    }
}