package com.example.basicfirebase

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class EditProfile : AppCompatActivity() {

    val firebaseFirestone= FirebaseFirestore.getInstance()


    @SuppressLint("MissingInflatedId", "CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)


        val editPhone = findViewById<EditText>(R.id.editNumber)
        val editName = findViewById<EditText>(R.id.editName)
        val editEmail = findViewById<EditText>(R.id.editEmail)

        val regSharePreferences = getSharedPreferences("register", Context.MODE_PRIVATE)
        val docid = regSharePreferences.getString("userId", "")

//        val getNumber = regSharePreferences?.getString("number", "")
//        val getName= regSharePreferences?.getString("name", "")
//        val getEmail= regSharePreferences?.getString("email", "")



        firebaseFirestone.collection("sahil").document(docid.toString()).get()
            .addOnSuccessListener {
                val getName = it.data?.get("name")
                val getEmail = it.data?.get("email")
                val getNumber = it.data?.get("number")

                editPhone.setText(getNumber.toString())
                editName.setText(getName.toString())
                editEmail.setText(getEmail.toString())
            }

        val button = findViewById<Button>(R.id.save)

        button.setOnClickListener {

            val allData = hashMapOf<String, Any>()
            allData["number"] = editPhone.text.toString()
            allData["name"] = editName.text.toString()
            allData["email"] = editEmail.text.toString()

            firebaseFirestone.collection("sahil").document(docid.toString()).update(allData)
                .addOnSuccessListener {
                    Toast.makeText(this, " Update Successful", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                }


            val intent = Intent(this, Profile::class.java)
            startActivity(intent)
            finish()


        }


    }
    }


