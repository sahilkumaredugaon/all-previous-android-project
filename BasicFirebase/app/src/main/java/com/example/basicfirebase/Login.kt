package com.example.basicfirebase

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.ktx.Firebase

class Login : AppCompatActivity() {

   private lateinit var  auth:FirebaseAuth

    private var requestCode = 1234

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth=FirebaseAuth.getInstance()





//            Login With Email and password

        val clickPhoneImage=findViewById<ImageView>(R.id.loginPhone)
        clickPhoneImage.setOnClickListener {
            val intent=Intent(this,PhoneActivity::class.java)
            startActivity(intent)
            Toast.makeText(this, "Login With Phone", Toast.LENGTH_SHORT).show()
            finish()
        }


        val registrationPage=findViewById<TextView>(R.id.registerText)
        registrationPage.setOnClickListener {
            val intent=Intent(this,MainActivity::class.java)
            Toast.makeText(this, "Registration First", Toast.LENGTH_SHORT).show()
            startActivity(intent)
            finish()
        }




        val userEmail=findViewById<EditText>(R.id.edit_email)
        val userPassword=findViewById<EditText>(R.id.edit_password)
        val loginButton=findViewById<Button>(R.id.loginButton)


        loginButton.setOnClickListener {
            auth.signInWithEmailAndPassword(userEmail.text.toString(),userPassword.text.toString())
                .addOnSuccessListener {
                    val intent=Intent(this,Profile::class.java)
                    Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show()
                    startActivity(intent)
                    finish()
                }
                .addOnFailureListener{
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }

        }






        //    Login With Google

            val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()


          val googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)

            val loginGoogle = findViewById<ImageView>(R.id.googleLogin)
        loginGoogle.setOnClickListener {
                val googleIntent = googleSignInClient.signInIntent
                startActivityForResult(googleIntent, requestCode)
            }

        }

        @Deprecated("Deprecated in Java")
        override fun onActivityResult(activityRequestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(activityRequestCode, resultCode, data)

            if (activityRequestCode == requestCode) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                task.addOnSuccessListener { it ->
                    val credencial = GoogleAuthProvider.getCredential(it.idToken, null)
                    auth.signInWithCredential(credencial)
                        .addOnSuccessListener {
                            startActivity(Intent(this, Profile::class.java))
                            Toast.makeText(this, "" + it.user?.displayName, Toast.LENGTH_SHORT)
                                .show()
                        }
                        .addOnFailureListener {
                            Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
                        }
                }
                    .addOnFailureListener {
                        Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()

                    }
            }





    }



}