package com.example.basicfirebase

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.google.firebase.firestore.FirebaseFirestore

class HomeFragment : Fragment() {

    val firebaseFirestone= FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("SuspiciousIndentation")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView= inflater.inflate(R.layout.fragment_home, container, false)
        val list=rootView.findViewById<ListView>(R.id.list_item)



            firebaseFirestone.collection("product").get()
                .addOnSuccessListener { document ->

                val dataList =
                    document.toObjects(DataModel::class.java) as ArrayList<DataModel>


                list.adapter = activity?.let { CustomList(dataList, it) }
            }
        return rootView
    }
}