package com.example.basicfirebase

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class ProfileFragment : Fragment() {
        val firebaseFirestone= FirebaseFirestore.getInstance()

    lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView= inflater.inflate(R.layout.fragment_profile_frament, container, false)

        auth=FirebaseAuth.getInstance()



        val showName=rootView.findViewById<TextView>(R.id.saveName)
        val showEmail=rootView.findViewById<TextView>(R.id.showEmail)
        val showPassword=rootView.findViewById<TextView>(R.id.showPassword)
        val showNumber=rootView.findViewById<TextView>(R.id.showNumber)

        val regSharePreferences = activity?.getSharedPreferences("register", Context.MODE_PRIVATE)
        val docid= regSharePreferences?.getString("userId", "")

                val  editPreferences = regSharePreferences?.edit()

                firebaseFirestone.collection("sahil").document(docid.toString()).get()
            .addOnSuccessListener {
                val name = it.data?.get("name")
                val email = it.data?.get("email")
                val number = it.data?.get("number")
                val password = it.data?.get("password")


                showName.text=name.toString()
                showEmail.text=email.toString()
                showNumber.text=number.toString()
                showPassword.text=password.toString()





        val button=rootView.findViewById<Button>(R.id.Edit)
        button.setOnClickListener {
            activity.let {
                val intent = Intent(it, EditProfile::class.java)



//            editPreferences?.putString("id",docid.toString())
//            editPreferences?.putString("number",number.toString())
//            editPreferences?.putString("name",name.toString())
//            editPreferences?.putString("email",email.toString())

            Toast.makeText(activity, " Welcome Edit Profile", Toast.LENGTH_SHORT).show()
            // save all added data in share pref
            editPreferences?.apply()
            startActivity(intent)
                activity?.finish()
        }

        }

            }


        val logoutButton=rootView.findViewById<Button>(R.id.LogOut)
        logoutButton.setOnClickListener {
            auth.signOut()

            val intent=Intent(activity,Login::class.java)
            startActivity(intent)
            activity?.finish()

            Toast.makeText(activity, "Logout Successful", Toast.LENGTH_SHORT).show()
        }

        return  rootView

}
}