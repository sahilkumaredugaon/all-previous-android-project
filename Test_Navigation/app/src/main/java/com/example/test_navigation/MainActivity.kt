package com.example.test_navigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       val home=HomeFragment()
        val notification=NotificationFragment()
        val setting=SettingFragment()

        setCurrentFragment(home)

        val button=findViewById<BottomNavigationView>(R.id.BottomNavigationView)

        button.setOnItemSelectedListener{
            when(it.itemId){
                R.id.home->setCurrentFragment(home)
                R.id.notifiction->setCurrentFragment(notification)
                R.id.setting->setCurrentFragment(setting)
            }
            true
        }
    }

    private fun setCurrentFragment(fragment: Fragment)=
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_Layout,fragment)
            commit()
        }


}