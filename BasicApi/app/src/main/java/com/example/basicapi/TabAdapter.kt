package com.example.basicapi

import android.widget.BaseAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class TabAdapter(fm:FragmentManager) :FragmentPagerAdapter(fm){
    override fun getCount(): Int {
       return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when(position){
            0->{
                return "CHATS"
            }
            1->{
                return "STATUS"
            }
            2->{
                return  "CALL"
            }
        }
        return super.getPageTitle(position)
    }

    override fun getItem(position: Int): Fragment {
       return when(position){
           0->{
               ChartFragment()
           }
           1->{
               StatusFragment()
           }

           else -> {
                CallFragment()
           }
       }
    }

}
