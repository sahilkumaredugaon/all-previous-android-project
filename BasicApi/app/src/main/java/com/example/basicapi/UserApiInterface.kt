package com.example.basicapi

import android.annotation.SuppressLint
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface UserApiInterface {

      @GET("/usersDetails")
     fun  getUserDetails(): Observable<List<UserDataModel>>


    companion object Factory{
    @SuppressLint("SuspiciousIndentation")
    fun create():UserApiInterface{
     val retrofit=Retrofit.Builder()
         .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
         .addConverterFactory(GsonConverterFactory.create())
         .baseUrl("https://demo0985251.mockable.io")
         .build()

         return retrofit.create(UserApiInterface::class.java)
    }
   }



}