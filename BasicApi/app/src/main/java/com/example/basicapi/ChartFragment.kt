package com.example.basicapi

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ChartFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root= inflater.inflate(R.layout.fragment_chart, container, false)


        val list=root.findViewById<ListView>(R.id.listView)

        UserApiInterface.create().getUserDetails()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe{ result->
                   list.adapter= activity?.let { CustomAdapter(result, it) }

                        list.setOnItemClickListener { parent, view, position, id ->
                            Toast.makeText(activity,result[position].Name+"\n"+result[position].Email, Toast.LENGTH_SHORT)
                                .show()

                    }
                    }

        return root
    }

}