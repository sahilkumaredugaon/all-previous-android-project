package com.example.basicapi

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class CustomAdapter(val userDetails:List<UserDataModel>, val context: Context):BaseAdapter(){
    override fun getCount(): Int {
      return userDetails.size
    }

    override fun getItem(position: Int): Any {
       return  userDetails[position]
    }

    override fun getItemId(position: Int): Long {
        return  position.toLong()
    }

    @SuppressLint("ViewHolder", "MissingInflatedId")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rootView=LayoutInflater.from(context).inflate(R.layout.adapter,parent,false)

        val image=rootView.findViewById<CircleImageView>(R.id.image)
        val name=rootView.findViewById<TextView>(R.id.name)
        val email=rootView.findViewById<TextView>(R.id.email)


        name.text=userDetails[position].Name
        email.text=userDetails[position].Email
        val url=userDetails[position].imageURL


        Glide.with(context)
            .load(url)
            .error(R.drawable.ic_launcher_background)
            .into(image)


        return rootView


    }
}