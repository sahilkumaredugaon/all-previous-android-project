package com.example.test_tab_layout

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class TabsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

 override fun getCount(): Int {
  return 3
 }

 override fun getPageTitle(poasition: Int): CharSequence? {
  when (poasition) {
   0 -> {
    return "CHATS"
   }
   1 -> {
    return " STATUS "
   }
   2 -> {
    return " CALLS "
   }

  }
  return super.getPageTitle(poasition)
 }

 override fun getItem(poasition: Int): Fragment {

  return when (poasition) {
   0 -> {
    ChartFragment()
   }
   1 -> {
    StatusFragment()
   }

   else -> {
    CallFragment()
   }

  }

 }



}
