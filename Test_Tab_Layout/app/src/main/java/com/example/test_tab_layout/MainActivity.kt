package com.example.test_tab_layout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewPager=findViewById<ViewPager>(R.id.viewPager_account)
        val tabLayout=findViewById<TabLayout>(R.id.tabLayout_account)

        viewPager.adapter=TabsAdapter(supportFragmentManager)

        tabLayout.setupWithViewPager(viewPager)

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.home -> {
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.setting -> {
                Toast.makeText(this, "Setting", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.group ->{
                Toast.makeText(this, "New Group", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.payment ->{
                Toast.makeText(this, "Payment", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.device ->{
                Toast.makeText(this, "Linked device", Toast.LENGTH_SHORT).show()
                true
            }
            else ->  super.onOptionsItemSelected(item)


        }
    }
}