package com.example.test_tab_layout

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import java.text.FieldPosition

class CustomAdapter(val dataList:ArrayList<CustomListModel>,val context: Context): BaseAdapter() {
  override fun getCount(): Int {
    return dataList.size
  }

  override fun getItem(position: Int): Any {
    return dataList[position]
  }

  override fun getItemId(position: Int): Long {
    return position.toLong()
  }
  @SuppressLint("MissingInflatedId", "ViewHolder")
  override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
    val rowView=LayoutInflater.from(context).inflate(R.layout.custom_item,parent,false)


    val name=rowView.findViewById<TextView>(R.id.name)
    val email=rowView.findViewById<TextView>(R.id.email)

    val imageView=rowView.findViewById<CircleImageView>(R.id.image)

    name.text=dataList[position].name
    email.text=dataList[position].email
    val url=dataList[position].imageURL

//    Glide.with(context).load(url).error(com.google.android.material.R.drawable.mtrl_ic_error).into(imageView)
    Glide.with(context)
      .load(url)
      .error(R.drawable.ic_launcher_background)
      .into(imageView)


    return rowView
  }


}