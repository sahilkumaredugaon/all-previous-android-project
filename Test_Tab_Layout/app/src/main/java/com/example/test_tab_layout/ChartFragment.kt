package com.example.test_tab_layout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView

class ChartFragment : Fragment() {

    val customDataList=DataList.details

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView= inflater.inflate(R.layout.fragment_chart, container, false)

        val list=rootView.findViewById<ListView>(R.id.listView)
        list.adapter=activity?.let { CustomAdapter(customDataList, it )}!!

        return rootView
    }



}